<!DOCTYPE html>
<html>
<head>
	<title>Edit</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/staily.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/normalize.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/demo.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/component1.css')}}" />


</head>
<body>
	<form method="post" action="{{route('user/proyectos/edit')}}">
		{{csrf_field()}}
	<div class="container">
		   <div class="row">
        <div class="col-md-12 ">     
           	<label>Id</label>
             <input type="text" name="id" value="{{$Proyecto->id}}" class="input2">
        </div>
     
    </div>
      <div class="row">
         <div class="col-md-12 ">
         	<label>Titulo</label>
       		<input type="text" name="Titulo" value="{{$Proyecto->Titulo}}" class="input2">
       </div>
     
    </div>
    <div class="row">
         <div class="col-md-12 ">
         	<label>SubTitulo</label>
       		<input type="text" name="SubTitulo" value="{{$Proyecto->SubTitulo}}" class="input2">
       </div>
     
    </div>
    <div class="row">
         <div class="col-md-12 ">
         	<label>Decriccion</label>
       		<input type="text" name="Desccricion" value="{{$Proyecto->Desccricion}}" class="input2 ancho"> 
       </div>
     
    </div>
		<input type="submit" name="" value="guardar">
	</div>
</form>
</body>
</html>	