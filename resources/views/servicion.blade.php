<!DOCTYPE html>
<html>
<head>
	<title>Servicios</title>
  <link rel="stylesheet" type="text/css" href="{{ asset('css/staily.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/normalize.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/demo.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/component1.css')}}" />
</head>
<body>
<div class="container">
	 <div class="fondo2">
           <img src="{{asset('images/servicios.jpg')}}">
           <img src="{{asset('images/servicios2.jpg')}}">
           <img src="{{asset('images/servicios3.jpg')}}">

           <div class="center" >           
              <h1 >Servicios</h1>            
            </div>
     </div>
     <div class="row">
       <div class="diseño col-md-4 ">
         <h2>Diseño de pagina</h2>
         <p>El diseño web es una actividad que consiste en la planificación, diseño e implementación de sitios web.  Un diseñador web tiene que ver con cómo crear y desarrollar un pagina web así también como los clientes interactúan con ella. Los buenos diseñadores web saben cómo poner juntos los principios de diseño para crear un sitio que se vea muy bien. También entienden acerca de la usabilidad y cómo crear un sitio que los clientes quieren navegar alrededor de porque es tan fácil de hacer.</p>

       </div>
       <div class="diseño col-md-4 ">
          <h1>Analisis de sistemas</h1>
         <p>El análisis de sistemas es la ciencia encargada del análisis de sistemas grandes y complejos, y la interacción entre los mismos. Esta área se encuentra muy relacionada con la investigación operativa. También se denomina análisis de sistemas a una de las etapas de construcción de un sistema informático, que consiste en relevar la información actual y proponer los rasgos generales de la solución futura.</p>
       </div>
       <div class="diseño col-md-4 ">
        <h1>Java</h1>
         <p>Java es un lenguaje de programación y una plataforma informática comercializada por primera vez en 1995 por Sun Microsystems. Hay muchas aplicaciones y sitios web que no funcionarán a menos que tenga Java instalado y cada día se crean más. Java es rápido, seguro y fiable. Desde portátiles hasta centros de datos, desde consolas para juegos hasta súper computadoras, desde teléfonos móviles hasta Internet, Java está en todas partes.</p>
       </div>
            </div>
     <div class="row">
       <div class="diseño col-md-4 ">
        <h1>Php</h1>
        <p>PHP (acrónimo recursivo de PHP: Hypertext Preprocessor) es un lenguaje de código abierto muy popular especialmente adecuado para el desarrollo web y que puede ser incrustado en HTML. </p>
       </div>
       <div class="diseño col-md-4 ">
        <h1>Laravel</h1>
        <p>Laravel es un nuevo y poderoso Framework PHP desarrollado por Taylor Otwell, que promete llevar al lenguaje PHP a un nuevo nivel. Laravel, propone una forma de desarrollar aplicaciones web de un modo mucho más ágil. Por ejemplo, en Laravel opcionalmente podemos usar el patrón de diseño MVC (Modelo-Vista-Controlador) tradicional, donde al igual que otros fameworks PHP, el controlador es programado como una clase. 
Por lo tanto, un Controlador es una clase PHP que dispone de métodos públicos que son el punto de entrada final de una petición HTTP (Request PHP) a nuestra aplicación. Pero, Lavarel propone además una forma distinta y más directa de responder a la solicitud HTTP, que veremos enseguida.</p>
       </div>
        <div class="diseño col-md-4 ">
        <h1>Bases de datos</h1>
        <p>Una base de datos es un “almacén” que nos permite guardar grandes cantidades de información de forma organizada para que luego podamos encontrar y utilizar fácilmente. A continuación te presentamos una guía que te explicará el concepto y características de las bases de datos.</p>
       </div>

       
     </div>
      
       <div class="component">
        <!-- Start Nav Structure -->
        <button class="cn-button" id="cn-button">+</button>
        <div class="cn-wrapper" id="cn-wrapper">
            <ul>
              <li><a href="#"><span class="icon-user"></span></a></li>
              <li><a href="{{asset('clientes/show')}}"><span class="icon-phone"></span></a></li>
              <li><a href="../"><span class="icon-home"></span></a></li>
              <li><a href="{{asset('clientes/proyectos')}}"><span class="icon-book"></span></a></li>
              <li><a href="#"><span class="icon-check"></span></a></li>
             </ul>
        </div>
        <div id="cn-overlay" class="cn-overlay"></div>
        <!-- End Nav Structure -->
      </div>
</div>
     <footer>              
            <div class="row">
        <div class="col-md-4 ">     
            <img src="{{asset('images/phone.png')}}">        
            <h4><span class="glyphicon glyphicon-earphone"></span>+57 5993170</h4>   
            <h4><span class="glyphicon glyphicon-earphone"></span>312 5105930</h4>   
             
        </div>
        <div class="col-md-4 " >
              <img src="{{asset('images/Internet.png')}}"> 
              <h4><span class="glyphicon glyphicon-envelope">diegolucas001@gmail.com</span></h4>
              <h4><span class="glyphicon glyphicon-envelope">diegolucas_00@hotmail.com</span></h4>
        </div> 
        <div class="col-md-4 ">
             <img src="{{asset('images/facebbok.png')}}"> 
             <h4><span class="glyphicon glyphicon-envelope"><a id="face" href="https://www.facebook.com/diego.cardona.7927">Diego cardona</a> </span></h4>
        </div>
    </div>

        
    </footer>
    <script src="{{asset('js/polyfills.js')}}"></script>
    <script src="{{asset('js/demo1.js')}}"></script>
    <script src="{{asset('js/modernizr-2.6.2.min.js')}}"></script>
</body>
</html>