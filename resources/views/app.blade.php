<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/staily.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/normalize.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/demo.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/component1.css') }}" />
</head >
</head>
<body>    
    <div class="containertop">
        
        
        <div class="fondo">
            <div id="center">           
              <h1 >Diego Fernando Cardona Rodriguez</h1>            
            </div>
        </div>
    
    <div class="container">
        <h1>¿Quien soy?</h1>
        <p>Soy un prgramador que le gusta hacer paginas web. En esta pagina conoseras mi trabajo y si deseas hacer una pagina web puedes contatarme</p>
        <h2>¿Ques es una pagina web?</h2>
        <p>Una página web, o página electrónica, página digital, o ciberpágina​​ es un documento o información electrónica capaz de contener texto, sonido, vídeo, programas, enlaces, imágenes, y muchas otras cosas, adaptada para la llamada World Wide Web (WWW) y que puede ser accedida mediante un navegador web.</p>
        <h2>páginas web más visitadas </h2>
        
    </div>
    <div class="row">
        <div class="col-md-4 azul">                    
            <h3>Google</h3>        
             <img src="{{asset('images/google.png')}}"> 
             <p>Siendo el buscador más popular del mundo, es lógico que esté a la cabeza de las páginas web más visitadas. Google permite a los usuarios de Internet encontrar al momento información de cualquier tema, incluyendo páginas web, imágenes, vídeos, ofertas, etcétera.</p>
             
        </div>
        <div class="col-md-4 verde" >
            <h3>Facebook</h3>                        
            <img src="{{asset('images/facebbok.png')}}">         
            <p>La red social por excelencia es también una de las páginas web más visitadas del mundo. Algunos medios apuntan a que Facebook ha llegado a superar en un par de ocasiones a Facebook en tráfico de usuarios. Y es que si la página creada por Mark Zuckerberg es una de las más visitadas es porque es capaz de conectar a personas de todo el mundo.</p>        
        </div> 
        <div class="col-md-4 rojo">
            <h3>Yahoo</h3>                        
            <img src="{{asset('images/yahoo.jpg')}}">         
            <p>Este buscador, que al igual que Google cuenta con una interfaz de email gratuita, llega hasta la mitad del ranking de las webs más visitadas. Yahoo! ofrece a los usuarios buscar cualquier tipo de información, ya sean páginas, imágenes, vídeos, noticias, etcétera. Además cuenta con contenido personalizable, correo electrónico, clubes y buscapersonas.</p>        
        </div>
    </div>

    </div>
   <div class="component">
                <!-- Start Nav Structure -->
                <button class="cn-button" id="cn-button">+</button>
                <div class="cn-wrapper" id="cn-wrapper">
                    <ul>
                      <li><a href="#"><span class="icon-picture"></span></a></li>
                      <li><a href="#"><span class="icon-headphones"></span></a></li>
                      <li><a href="#"><span class="icon-home"></span></a></li>
                      <li><a href="#"><span class="icon-facetime-video"></span></a></li>
                      <li><a href="#"><span class="icon-envelope-alt"></span></a></li>
                     </ul>
                </div>
                <div id="cn-overlay" class="cn-overlay"></div>
                <!-- End Nav Structure -->
            </div>
    <footer>              
            <div class="row">
        <div class="col-md-4 ">     
            <img src="{{asset('images/phone.png')}}">        
            <h4><span class="glyphicon glyphicon-earphone"></span>+57 5993170</h4>   
            <h4><span class="glyphicon glyphicon-earphone"></span>312 5105930</h4>   
             
        </div>
        <div class="col-md-4 " >
              <img src="{{asset('images/Internet.png')}}"> 
              <h4><span class="glyphicon glyphicon-envelope">diegolucas001@gmail.com</span></h4>
              <h4><span class="glyphicon glyphicon-envelope">diegolucas_00@hotmail.com</span></h4>
        </div> 
        <div class="col-md-4 ">
             <img src="{{asset('images/facebbok.png')}}"> 
             <h4><span class="glyphicon glyphicon-envelope"><a id="face" href="https://www.facebook.com/diego.cardona.7927">Diego cardona</a> </span></h4>
        </div>
    </div>
        
    </footer>
    <script src="{{asset('js/polyfills.js')}}"></script>
    <script src="{{asset('js/demo1.js')}}"></script>
    <script src="{{('js/modernizr-2.6.2.min.js')}}"></script>
</body>
</html>