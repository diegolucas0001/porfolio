<!DOCTYPE html>
<html>
<head>
	<title>Lista de Proyectos</title>
	 <link rel="stylesheet" type="text/css" href="{{ asset('css/staily.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
</head>
<body class="fuego">
	<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Brand</a>
    </div>
</div>
</nav>
	<div class="container">

		<div class="dragonrojo">
			<h1>Proyectos</h1>
		</div>
		<table class="table">
			<tr>
				<td>
					<label>Id</label>
				</td>
				<td>
					<label>Titulo</label>
				</td>
				<td>
					<label>SubTitulo</label>
				</td>
				
			</tr>
			@foreach($proyectos as $proyecto)
			<tr>
				<td><label>{{$proyecto->id}}</label></td>
			
				<td><label>{{$proyecto->Titulo}}</label></td>
			
				<td><label>{{$proyecto->SubTitulo}}</label></td>
			
			
				<td><button class="btn btn-primary"><label><a href="{{ route('user/eliminar', ['id'=> $proyecto->id])}}"> Eliminar </a></label></button></td>
				<td><button class="btn btn-primary"><label><a href="{{ route('user/update', ['id'=> $proyecto->id])}}"> Edit </a> </label></button></td>

			</tr>
			 @endforeach
		</table>
		<td><button class="btn btn-primary"><label><a href="{{ route('proyectos/create')}}"> Crear </a> </label></button></td>
	</div>
</body>
</html>