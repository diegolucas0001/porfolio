<!DOCTYPE html>
<html>
<head>
	<title>Contactos</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/staily.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/normalize.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/demo.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/component1.css') }}" />
</head >
</head>
<body>

<div class="container">
  <div class="fondo1">
            <div id="center">           
              <h1 >Contactos</h1>            
            </div>
        </div>
   <table class="table">
     <tr>
       <td class="success">
         <label>Nombre completo</label>
       </td>
       <td>
         <input type="text" name="Nombre" class="form-control">
       </td>
     </tr>
     <tr>
       <td class="success">
         <label>Telefono</label>
       </td>
        <td>
         <input type="number" name="Telefono" class="form-control">
       </td>
     </tr>
     <tr>
       <td class="success">
         <label>Direccion</label>
       </td>
       <td>
         <input type="text" name="Direccion" class="form-control">
       </td>
      </tr>
      <tr>
       <td class="success">
         <label>Correo</label>
       </td>
       <td>
         <input type="email" name="Correo" class="form-control">
       </td>
      </tr>
      <tr>
       <td class="success">
         <label>Descriccion</label>
       </td>

       <TD>
         <textarea rows="5" class="form-control"></textarea>
       </TD>
     </tr>
    
   </table>
</div>

 <div class="component">
        <!-- Start Nav Structure -->
        <button class="cn-button" id="cn-button">+</button>
        <div class="cn-wrapper" id="cn-wrapper">
            <ul>
              <li><a href="#"><span class="icon-user"></span></a></li>
              <li><a href="#"><span class="icon-phone"></span></a></li>
              <li><a href="{{asset('../')}}"><span class="icon-home"></span></a></li>
              <li><a href="{{asset('clientes/proyectos')}}"><span class="icon-book"></span></a></li>
              <li><a href="{{asset('clientes/servicios')}}"><span class="icon-check"></span></a></li>
             </ul>
        </div>
        <div id="cn-overlay" class="cn-overlay"></div>
        <!-- End Nav Structure -->
      </div>
             <footer>              
            <div class="row">
        <div class="col-md-4 ">     
            <img src="{{asset('images/phone.png')}}">        
            <h4><span class="glyphicon glyphicon-earphone"></span>+57 5993170</h4>   
            <h4><span class="glyphicon glyphicon-earphone"></span>312 5105930</h4>   
             
        </div>
        <div class="col-md-4 " >
              <img src="{{asset('images/Internet.png')}}"> 
              <h4><span class="glyphicon glyphicon-envelope">diegolucas001@gmail.com</span></h4>
              <h4><span class="glyphicon glyphicon-envelope">diegolucas_00@hotmail.com</span></h4>
        </div> 
        <div class="col-md-4 ">
             <img src="{{asset('images/facebbok.png')}}"> 
             <h4><span class="glyphicon glyphicon-envelope"><a id="face" href="https://www.facebook.com/diego.cardona.7927">Diego cardona</a> </span></h4>
        </div>
    </div>
        
    </footer>
    <script src="{{asset('js/polyfills.js')}}"></script>
    <script src="{{asset('js/demo1.js')}}"></script>
    <script src="{{asset('js/modernizr-2.6.2.min.js')}}"></script>
</body>
</html>