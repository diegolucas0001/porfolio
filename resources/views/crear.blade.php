<!DOCTYPE html>
<html>
<head>
	<title>Crear</title>
	    <link rel="stylesheet" type="text/css" href="{{ asset('css/staily.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/normalize.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/demo.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/component1.css')}}" />
</head>
<body>
	<div class="container">
    {{Form::open(  ['route' => ['proyectos/store'],'method' => 'POST', 'id' => 'formStore'] ) }}
		<table class="table">
     <tr>
       <td class="success">
         <label>Titulo</label>
       </td>
       <td>
         {{Form::text('Titulo','', array('class'=> 'form-control','id'=> 'Titulo', 'placeholder'=>'Ingresa el titulo'))}}  
       </td>
     </tr>
     <tr>
       <td class="success">
         <label>subtitulo</label>
       </td>
        <td>
         {{Form::text('SubTitulo','', array('class'=> 'form-control','id'=> 'SubTitulo', 'placeholder'=>'Ingresa el subtitulo'))}}
       </td>
     </tr>
    
      <tr>
       <td class="success">
         <label>Descriccion</label>
       </td>

       <TD>
         {{Form::text('Descriccion','', array('class'=> 'form-control','id'=> 'Descriccion', 'placeholder'=>'Ingresa la descriccion'))}}
       </TD>
     </tr>
    
   </table>
   {{Form::submit('Crear proyecto',  array('class' => 'btn btn-success'))}}
   {{Form::close() }}
	</div>
</body>
</html>