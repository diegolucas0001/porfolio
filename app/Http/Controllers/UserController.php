<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Proyecto as proyectos;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    public function login()
    {
          return view('login');
    }
    public function proyectos()
    {
    	 $proyectos=proyectos::orderBy('id','des')->get();
          return view('proyectos')->with('proyectos',$proyectos);
    }
     public function update($id)

    {
    	$Proyecto = proyectos::find($id);
    	return \View::make('Edit', compact('Proyecto')); 
    }

    public function edit(Request $request)
    {
     $proyecto = proyectos::find($request->id);
     $proyecto->Titulo = $request->Titulo;
     $proyecto->SubTitulo = $request->SubTitulo;
     $proyecto->Desccricion = $request->Desccricion;

     $proyecto->save();

          return Redirect('user/proyectos');

    }
    public function eliminar($id)
    {
    $proyecto = proyectos::find($id);
    $proyecto->delete();
    return \View::make('proyectos');

    }
}

