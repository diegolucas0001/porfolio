<?php

Route::get('/', function () {
    return view('welcome');
});
//Route::resource('clientes', 'ClientesController');

Route::get('clientes/show',[

 	'as'=> 'clientes/show',
 	'uses' => 'ClientesController@show'
 	 


 	]);

Route::get('clientes/home',[
 	'as'=> 'clientes/home',
 	

 	]);

/*
 Route::get('clientes/update/{id}',[
 	'as'=> 'clientes.update',
 	'uses'=> 'ClientesController@update'


 	]);
*/
Route::get('clientes/servicios',[
 	'as'=> 'clientes/servicios',
 	'uses' => 'ClientesController@servicios'
 	]);
Route::get('clientes/proyectos',[
 	'as'=> 'clientes/proyectos',
 	'uses' => 'ClientesController@proyectos'
 	]);
Route::get('clientes/edit/{id}',[
 	'as'=> 'clientes/edit',
 	'uses' => 'ClientesController@edit'
 	]);


Route::get('user/login',[
 	'as'=> 'user/login',
 	'uses' => 'UserController@login'
 	]);
Route::get('user/proyectos',[
 	'as'=> 'user/proyectos',
 	'uses' => 'UserController@proyectos'
 	]);
Route::post('user/proyectos/edit',[
 	'as'=> 'user/proyectos/edit',
 	'uses' => 'UserController@edit'
 	]);
Route::get('user/update/{id}',[
 	'as'=> 'user/update',
 	'uses' => 'UserController@update'
 	]);
Route::post('user/eliminar/{id}',[
 	'as'=> 'user/eliminar',
 	'uses' => 'UserController@eliminar'
 	]);

Route::post('proyectos/store',[
 	'as'=> 'proyectos/store',
 	'uses' => 'ProyectosController@store'
 	]);
Route::get('proyectos/create',[
 	'as'=> 'proyectos/create',
 	'uses' => 'ProyectosController@create'
 	]);